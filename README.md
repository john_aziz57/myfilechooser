# README #

MyFileChooser is a simple file chooser and directory chooser.

### What is this repository for? ###

* This is a simple library that allow you to choose files and directories. all you have to do is import the jar the file and some simple code and it will work seamlessly or I hope so.
### Why I did it ###
I have a program that I wanted the user to do both selecting files and selecting directories; the libraries that I found offered different UI for each action ( selecting file / folders) which is not visually suitable for the user. that is why i put both of them into one library
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* How to set up the library in your project
download the jar [file](https://bitbucket.org/john_aziz57/myfilechooser/src/d705c823306c093391d34db3b0b4d32b3b760aec/bin/myfilechooser.jar?at=master) 
import the jar file into your project
add the following lines into you manifest file

        <activity
            android:name="com.john_aziz57.myfilechooser.MainActivity"
            android:exported="true" >
        </activity>

since you will probably will read or write files don't forget to add the appropriate permissions

the place where you want to start the activity add the following code
to select folders

		Intent i = new Intent(this,
				com.john_aziz57.myfilechooser.MainActivity.class);
		i.putExtra(MainPresenter.TAG, MainPresenter.SELECT_FOLDER);
		startActivityForResult(i, FOLDER_SELECT_CODE);

to select files

		Intent i = new Intent(this,
				com.john_aziz57.myfilechooser.MainActivity.class);
		i.putExtra(MainPresenter.TAG, MainPresenter.SELECT_FILE);
		startActivityForResult(i, FILE_SELECT_CODE);

in onActivityResult add the following code

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case FILE_SELECT_CODE:
			if (resultCode == RESULT_OK) {
				// Get the Uri of the selected file
				Uri uri = data.getData();
				// Get the path
				String path = uri.getPath();
                                .......

### Final Notes ###
I tried to use MVP pattern as much as I could and AsyncTaskLoader. I learnt a lot from this [project ](https://github.com/iPaulPro/aFileChooser)without it I wouldn't be able to write this simple library.