package com.john_aziz57.myfilechooser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class FileArrayAdapter extends BaseAdapter{

	    private final LayoutInflater mInflater;

	    private List<File> mData = new ArrayList<File>();

	    public FileArrayAdapter(Context context) {
	        mInflater = LayoutInflater.from(context);
	    }

	    public void add(File file) {
	        mData.add(file);
	        notifyDataSetChanged();
	    }

	    public void remove(File file) {
	        mData.remove(file);
	        notifyDataSetChanged();
	    }

	    public void insert(File file, int index) {
	        mData.add(index, file);
	        notifyDataSetChanged();
	    }

	    public void clear() {
	        mData.clear();
	        notifyDataSetChanged();
	    }

	    @Override
	    public File getItem(int position) {
	        return mData.get(position);
	    }

	    @Override
	    public long getItemId(int position) {
	        return position;
	    }

	    @Override
	    public int getCount() {
	        return mData.size();
	    }

	    public List<File> getListItems() {
	        return mData;
	    }

	    /**
	     * Set the list items without notifying on the clear. This prevents loss of
	     * scroll position.
	     *
	     * @param data
	     */
	    public void setListItems(List<File> data) {
	        mData = data;
	        notifyDataSetChanged();
	    }

	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        View row = convertView;

	        if (row == null)// he is checking if the row is made or not
	            row = mInflater.inflate(R.layout.file_folder_row, parent, false);
	       
	        TextView txt = (TextView) row.findViewById(R.id.tv_file_folder);

	        // Get the file at the current position
	        final File file = getItem(position);

	        // Set the TextView as the file name
	        txt.setText(file.getName());
	        if(file.isFile())
	        	txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.file, 0, 0, 0);
	        else
	        	txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.folder, 0, 0, 0);
	        // If the item is not a directory, use the file icon

	        return row;
	    }

	}

