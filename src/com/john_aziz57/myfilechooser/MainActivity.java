package com.john_aziz57.myfilechooser;

import java.io.File;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends ActionBarActivity {

	int mMode = 0;

	public MainActivity(int mode) {
		mMode = mode;
	}
	
	public MainActivity() {
//		Bundle extras = getIntent().getExtras();
//		int value=MainPresenter.SELECT_FILE;
//		if (extras != null) {
//		    value = extras.getInt(MainPresenter.TAG);
//		}
//		mMode = value;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Bundle extras = getIntent().getExtras();
		int value=MainPresenter.SELECT_FILE;
		if (extras != null) {
		    value = extras.getInt(MainPresenter.TAG);
		}
		mMode = value;
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (savedInstanceState == null) {
			getSupportFragmentManager()
					.beginTransaction()
					.add(R.id.container,
							new MainFragment(new MainPresenter(this, this,
									mMode))).commit();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void finishWithResult(File result) {
		if (result != null)
			setResult(RESULT_OK, new Intent().setData(Uri.fromFile(result)));
		else
			setResult(RESULT_CANCELED, new Intent());
		finish();

	}

}
