package com.john_aziz57.myfilechooser;

import java.io.File;
import java.util.List;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;

/**
 * This class will be both Presenter and Model
 * 
 * @author John
 *
 */

public class MainPresenter implements
		LoaderManager.LoaderCallbacks<List<File>>, OnItemClickListener {
	public static final String TAG = "MODE";
	public static final int SELECT_FILE = 0;
	public static final int SELECT_FOLDER = 2;

	private int mMode = 0;
	private MainFragment mMainView;
	private String mCPath = "";// currrent path
	private Context mContext;
	private MainActivity mActivity;
	private List<File> mFiles;

	public MainPresenter(Context context, MainActivity activity,int mode) {
		mActivity = activity;
		mContext = context;
		mCPath = Environment.getExternalStorageDirectory().getAbsolutePath();// Default
																				// Value
		mMode = mode;
	}

	/**
	 * 
	 * @param mFragment
	 *            The fragment that is used right now to get the loaderManager
	 */
	public void getFiles(MainFragment mFragment) {
		mMainView = mFragment;
		if (mMainView.getCurrentPath() == null) {
			mMainView.setCurrentPath(mCPath);
		} else {
			mCPath = mMainView.getCurrentPath();
		}
		mFragment.getLoaderManager().restartLoader(0, null, this);
	}

	@Override
	public android.support.v4.content.Loader<List<File>> onCreateLoader(
			int arg0, Bundle arg1) {
		return new FilesLoader(mContext, mCPath);
	}

	@Override
	public void onLoadFinished(
			android.support.v4.content.Loader<List<File>> arg0, List<File> arg1) {
		mFiles = arg1;
		mMainView.setAdapterData(arg1);
		// mMainView.getLoaderManager().destroyLoader(0);
	}

	@Override
	public void onLoaderReset(android.support.v4.content.Loader<List<File>> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		File f = mFiles.get(position);

		// according to the mode it will be decided what will happen with this
		// item

		if (f.isDirectory()) {// if directory open the directory
			System.out.println("Is Directory " + f.isDirectory());
			System.out.println("Path " + f.getPath());
			//

			//
			mCPath = f.getPath();

			FragmentManager fm = mActivity.getSupportFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();
			ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			ft.replace(R.id.container, new MainFragment(this));
			ft.addToBackStack(mCPath);
			ft.commit();

		} else {// if file
			if (mMode == SELECT_FILE)// if file and select mode is file
			{
				System.out.println(f.getPath());
				finishWithResult(f);
			}
			//TODO finish
		}

	}

	public String getCurrentPath() {
		return mCPath;
	}

	public int getMode() {
		return mMode;
	}

	public void onSelectPathButtonClick(View v) {
		// TODO finish
		System.out.println("Current Path " + mCPath);
		finishWithResult(new File(mCPath));
	}
	
	public void finishWithResult(File result){
		mActivity.finishWithResult(result);
	}

}
