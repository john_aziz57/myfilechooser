package com.john_aziz57.myfilechooser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;


public class FilesLoader extends AsyncTaskLoader<List<File>>
{
	String mPath;
	boolean ready = false;
	private ArrayList<File> result;
	public FilesLoader(Context context, String path) {
		super(context);
		mPath = path;
		ready=false;
	}

	@Override
	public List<File> loadInBackground() {
		if(android.os.Debug.isDebuggerConnected())
		    android.os.Debug.waitForDebugger();
		
		
		for(int i=0;i<2;i++){
			System.out.println("Files ___ "+mPath);
		}
		
		File dir = new File( mPath);
//		File listFiles [] =dir.listFiles();
//		// TODO throw exception
		Log.v("Files",dir.exists()+"");
		Log.v("Files",dir.isDirectory()+"");
		Log.v("Files",dir.isHidden()+"");
//		
		result = new ArrayList<File>();
//		
		for(File f : dir.listFiles()){
			if(!f.isHidden())
				result.add(f);
		}
//		
		
		ready = true;
		return result;
	}
	
	@Override
	protected void onStartLoading() {
		if(android.os.Debug.isDebuggerConnected())
		    android.os.Debug.waitForDebugger();
		  if(ready) {
		    deliverResult(result);
		    ready=!ready;
		  } else {
		    forceLoad();
		  }
		}
	
	

	@Override
	public void deliverResult(List<File> data) {
		if(android.os.Debug.isDebuggerConnected())
		    android.os.Debug.waitForDebugger();
		if (isReset()) {
			return;
		}
		if (isStarted())
			super.deliverResult(data);

	}

}
