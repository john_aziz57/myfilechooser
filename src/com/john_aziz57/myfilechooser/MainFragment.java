package com.john_aziz57.myfilechooser;

import java.io.File;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

//@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MainFragment extends Fragment {

	private View rootView;
	private MainPresenter mp;
	private FileArrayAdapter fileAdapter;
	private String cPath;

	public MainFragment(MainPresenter mainPresenter) {
		mp = mainPresenter;
	}

	public String getCurrentPath() {
		return cPath;
	}

	public void setCurrentPath(String path) {
		cPath = path;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_main, container, false);

		ListView lv = (ListView) rootView.findViewById(R.id.lv_files);

		Button selPath = (Button) rootView.findViewById(R.id.b_select_path);

		switch (mp.getMode()) {// if we are selecting file don't show the select
								// path button
		case MainPresenter.SELECT_FILE:
			selPath.setVisibility(View.GONE);
			break;
		case MainPresenter.SELECT_FOLDER:
			selPath.setVisibility(View.VISIBLE);
			selPath.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					mp.onSelectPathButtonClick(v);
					
				}
			});
			break;
		}

		fileAdapter = new FileArrayAdapter(getActivity());
		lv.setAdapter(fileAdapter);
		lv.setOnItemClickListener(mp);
		mp.getFiles(this);
		return rootView;
	}

	public void setAdapterData(List<File> list) {
		// This folder is empty
		if (list.size() == 0) {
			TextView txt = (TextView) rootView.findViewById(R.id.tv_empty_msg);
			txt.setVisibility(TextView.VISIBLE);
		}
		fileAdapter.setListItems(list);
	}

}